#!/bin/sh

GMAILPASSWD=$1
LN=$2
LD=$3

mkdir -p upgrade-helper
echo "[settings]" > upgrade-helper/upgrade-helper.conf
echo "smtp=smtp.gmail.com:465" >> upgrade-helper/upgrade-helper.conf
echo "email_client_pswd=${GMAILPASSWD}" >> upgrade-helper/upgrade-helper.conf
echo "global_maintainer_override=akuster808@gmail.com" >> upgrade-helper/upgrade-helper.conf
echo "status_recipients=akuster808@gmail.com" >> upgrade-helper/upgrade-helper.conf
echo "from=akuster808@gmail.com" >> upgrade-helper/upgrade-helper.conf
echo "layer_mode=yes" >> upgrade-helper/upgrade-helper.conf
echo "layer_name=${LN}" >> upgrade-helper/upgrade-helper.conf
echo "layer_dir=${LD}" >> upgrade-helper/upgrade-helper.conf
echo "layer_machines=qemux86" >> upgrade-helper/upgrade-helper.conf
